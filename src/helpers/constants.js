export const statuses = {
    LISTEN: 'listen',
    REQUEST: 'request',
    SUCCESS: 'success',
    ERROR: 'error'
}