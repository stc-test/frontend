export const llAccessTokenHelper = {
    get: () => localStorage.getItem('accessToken'),
    set: (token) => localStorage.setItem('accessToken', token),
    remove: () => localStorage.removeItem('accessToken')
}

export default [
    llAccessTokenHelper
]