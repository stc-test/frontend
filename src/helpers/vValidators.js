import {
    helpers,
    email as emailValidator,
    required as requiredValidator,
    minLength as minLengthValidator,
    maxLength as maxLengthValidator,
    sameAs as sameAsValidator
} from '@vuelidate/validators'

export const minLength = (len) => helpers.withMessage(({ $params }) => {
    return `Данное поле должно содержать минимум ${$params.min} символа(ов)`
}, minLengthValidator(len) )

export const maxLength = (len) => helpers.withMessage(({ $params }) => {
    return `Данное поле должно содержать максимум ${$params.max} символа(ов)`
}, maxLengthValidator(len) )

export const email = helpers.withMessage(`Пожалуйста введите правильный почтовый адрес`, emailValidator)
export const required = helpers.withMessage('Это поле не может быть пустым', requiredValidator)

export const letters = helpers.withMessage(`Поле не должно содержать цифр`, (value) => {
    let pattern = /[0-9]/
    return !helpers.req(value) || !value.match(pattern)
})

export const sameAsPassword = (value) => {
    console.log(value)
    return helpers.withMessage('Пароли не совпадают', sameAsValidator(value))
}

