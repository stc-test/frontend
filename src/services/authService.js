import $api from '@/http/api'
import apiEndpoints from '@/config/apiEndpoints'

export default new class AuthService {
    async registration(form) {
        return await $api.post(apiEndpoints.auth.registration, form)
    }

    async login(form) {
        return await $api.post(apiEndpoints.auth.login, form)
    }

    async logout() {
        return await $api.post(apiEndpoints.auth.logout)
    }
}