export default {
    auth: {
        registration: '/auth/registration',
        login: '/auth/login',
        logout: '/auth/logout',
        refresh: '/auth/refresh',
        authUser: '/auth/user'
    }
}