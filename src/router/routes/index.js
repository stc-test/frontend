import guestMiddleware from '@/router/middlewares/guest.middleware';
import authMiddleware from '@/router/middlewares/auth.middleware';
import routesNames from '@/router/routesNames'
export default [
    {
        path: '/',
        redirect: 'registration'
    },
    {
        path: '/registration',
        name: routesNames.registration,
        component: () => import('@/views/Registration.vue'),
        meta: {
            middleware: [
                guestMiddleware
            ]
        }
    },
    {
        path: '/login',
        name: routesNames.login,
        component: () => import('@/views/Login.vue'),
        meta: {
            middleware: [
                guestMiddleware
            ]
        }
    },
    {
        path: '/dashboard',
        name: routesNames.dashboard,
        component: () => import('@/views/Dashboard.vue'),
        meta: {
            middleware: [
                authMiddleware
            ]
        }
    },
]