import { createRouter, createWebHistory } from 'vue-router'
import store from '@/store/';
import rts from '@/router/routes'
import middlewarePipeline from './middlewarePipeline';

(async () => {
    await store.dispatch('authModule/refresh');
})()

const routes = [
    ...rts
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {

    if (!to.meta.middleware) {
        return next();
    }

    const middleware = to.meta.middleware
    const context = {
        to,
        from,
        next,
        store
    }
    return middleware[0]({
        ...context,
        next: middlewarePipeline(context, middleware, 1)
    })
})

export default router
