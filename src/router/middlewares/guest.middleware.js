import routesNames from '@/router/routesNames'

export default ({ next, store }) => {

    const authState = store.getters[`authModule/authState`]
    if(authState) {
        return next({
            name: routesNames.dashboard,
        })
    }

    return next()
}