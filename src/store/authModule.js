import { API_URL } from '@/http/api';
import AuthService from "@/services/authService";
import axios from "axios";
import { llAccessTokenHelper } from '@/helpers/auth';
import { statuses } from '@/helpers/constants';

async function authWrapper({commit}, data, callback, {withError = true} = {}) {
    commit('_setStatus', statuses.REQUEST);
    try {
        const response = await callback(data);
        commit('_setError', null)
        commit('_setStatus', statuses.SUCCESS);
        commit('_setUser', response.data.user);
        commit('_setToken', response.data.accessToken);
    }
    catch(error) {
        console.log(error);
        if(withError) {
            commit('_setError', error)
        }

        commit('_setStatus', statuses.ERROR);
        commit('_setUser', null);
        commit('_setToken', null);
    }
}

export default {
    namespaced: true,
    state: () => ({
        status: statuses.LISTEN,
        user: null,
        token: llAccessTokenHelper.get() || null,
        error: null
    }),
    mutations: {
        _setStatus(state, status) {
            state.status = status;
        },
        _setUser(state, payload) {
            state.user = payload;
        },
        _setToken(state, payload) {
            state.token = payload;
            if(payload) {
                return llAccessTokenHelper.set(payload);
            }
            llAccessTokenHelper.remove();
        },
        _setError(state, error) {
            state.error = error
        }
    },
    actions: {
        async registration(context, data) {
            await authWrapper(context, data, AuthService.registration);
        },
        async login(context, data) {
            await authWrapper(context, data, AuthService.login);
        },
        async logout(context) {
            await authWrapper(context, null, AuthService.logout);
        },
        async refresh(context) {
            await authWrapper(context, null, () => axios.get(`${API_URL}/auth/refresh`, { withCredentials: true }), {withError: false});
        }
    },
    getters: {
        authState: (state) => !!state.token,
        authUser: (state) => state.user,
        authError: (state) => state.error
    },
}