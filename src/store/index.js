import {createStore} from 'vuex'
import authModule from '@/store/authModule'

const store = createStore({
    modules: {
        authModule
    }
})

export default store